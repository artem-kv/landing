$(document).foundation();

$('.input').each(function () {
  var $t = $(this);
  var $label = $t.find('label');
  var $input = $t.find('input');

  $input.keypress(function () {
    $label.css({top: '-20px'});
  });
});
